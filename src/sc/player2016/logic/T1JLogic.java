package sc.player2016.logic;

import net.schwagereit.t1j.*;
import net.schwagereit.t1j.Move;
import net.schwagereit.t1j.Board;
import sc.player2016.Starter;
import sc.plugin2016.*;
import sc.shared.GameResult;

import java.util.ArrayList;

public class T1JLogic implements IGameHandler {
    /*
 * Klassenweit verfuegbarer Zufallsgenerator der beim Laden der klasse
 * einmalig erzeugt wird und darn immer zur Verfuegung steht.
 */
    private Starter client;
    private GameState gameState;
    private Match match;
    private Player currentPlayer;

    /**
     * Erzeugt ein neues Strategieobjekt, das zufaellige Zuege taetigt.
     *
     * @param client Der Zugrundeliegende Client der mit dem Spielserver
     *               kommunizieren kann.
     */
    public T1JLogic(Starter client) {
        this.client = client;
        this.match = null;
        CheckPattern.getInstance().loadPattern();
        Zobrist.getInstance().initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void gameEnded(GameResult data, PlayerColor color,
                          String errorMessage) {

        System.out.println("*** Das Spiel ist beendet");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRequestAction() {
        long startTime = System.nanoTime();
        if(match == null) initMatch();
        System.out.println("*** Es wurde ein Zug angefordert: current = " + this.match.getCurrentPlayer());
        FindMove.getFindMove().prepareSearch(this.match.getCurrentPlayer());
        Thread compute = new Thread(FindMove.getFindMove());
        compute.start();
        System.out.println(startTime + ":" + System.nanoTime());
        try { Thread.sleep(1700 - (System.nanoTime() - startTime) / 1000000); } catch(InterruptedException e) {}
        Move best = FindMove.getFindMove().getCurrentBestMove();

        System.out.println("*** sende zug: ");
        sc.plugin2016.Move selection = new sc.plugin2016.Move(best.getX(), best.getY());
        System.out.println("*** setze Strommast auf x="
                + selection.getX() + ", y="
                + selection.getY());
        sendAction(selection);
        compute.interrupt();
        try { compute.join(); } catch(InterruptedException e) {}
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onUpdate(Player player, Player otherPlayer) {
        currentPlayer = player;

        System.out.println("*** Spielerwechsel: " + player.getPlayerColor());

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onUpdate(GameState gameState) {
        this.gameState = gameState;

        currentPlayer = gameState.getCurrentPlayer();

        System.out.print("*** Das Spiel geht vorran: Zug = "
                + gameState.getTurn());
        System.out.println(", Spieler = " + currentPlayer.getPlayerColor());
        if(match != null) {
            sc.plugin2016.Move last = gameState.getLastMove();
            match.setlastMove(last.getX(), last.getY());

	    Board board = match.getBoardX();
	    Structure structure = new Structure(board);

	    for ( int row = 0; row < Board.SIZE; row += 1 ) {
		for ( int column = 0; column < Board.SIZE; column += 1 )
		    System.out.format("%+3.0f ", structure.pinValues[row][column]);
		System.out.format("%n");
	    }
	    Evaluation evaluation = board.getEval();
	    float influence = evaluation.evaluate();
	    System.out.format("%.5f%n", influence);
	    for ( int row = 0; row < Board.SIZE; row += 1 ) {
		for ( int column = 0; column < Board.SIZE; column += 1 )
		    System.out.format("%+6.2f ", evaluation.influenceValues[row][column]);
		System.out.format("%n");
	    }

        }
    }

    private void initMatch() {
        System.out.println("*** Initialize T1J match data structures");
        MatchData md = new MatchData();
        md.mdYstarts = true;
        md.mdXhuman = currentPlayer.getPlayerColor() != PlayerColor.BLUE;
        md.mdYhuman = currentPlayer.getPlayerColor() != PlayerColor.RED;
        ArrayList<Move> swamps = new ArrayList<>();
        for(int y = 0; y < 24; ++y) {
            for(int x = 0; x < 24; ++x) {
                if(gameState.getBoard().getField(x,y).getType() == FieldType.SWAMP) {
                    swamps.add(new Move(x,y));
                }
            }
        }

        this.match = new Match();
        match.prepareNewMatch(md, swamps);
        if(md.mdYhuman) {
            sc.plugin2016.Move last = gameState.getLastMove();
            match.setlastMove(last.getX(), last.getY());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendAction(sc.plugin2016.Move move) {
        client.sendMove(move);
    }
}
