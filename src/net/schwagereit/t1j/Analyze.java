package net.schwagereit.t1j;

import com.thoughtworks.xstream.XStream;
import sc.networking.FileSystemInterface;
import sc.networking.clients.XStreamClient;
import sc.plugin2016.FieldType;
import sc.plugin2016.GameState;
import sc.plugin2016.util.Configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;

@SuppressWarnings("ALL")
class Analyze {

    private static Match match;
    private static ArrayList<GameState> turns;

    private static void withSuppressStdout(Runnable action) {
        PrintStream out = System.out;
        System.setOut(new PrintStream(new OutputStream() {
            @Override
            public void write(int i) throws IOException {
            }
        }));
        action.run();
        System.setOut(out);
    }

    private static void withIndentStdout(Runnable action) {
        PrintStream out = System.out;
        System.setOut(new PrintStream(new OutputStream() {
            private boolean newLine = true;
            @Override
            public void write(int i) throws IOException {
                if(i != '\n' && newLine) out.print("   ");
                newLine = i == '\n';
                out.write(i);
            }
        }));
        action.run();
        System.setOut(out);
    }

    private static void loadReplay(String file) throws IOException, InterruptedException {
        turns = new ArrayList<>();
        XStream xstream = Configuration.getXStream();
        GZIPInputStream input = new GZIPInputStream(new FileInputStream(file));
        XStreamClient client = new XStreamClient(xstream, new FileSystemInterface(input)) {
            @Override
            protected void onDisconnect(DisconnectCause cause) {
                synchronized (turns) { turns.notify(); }
            }

            @Override
            protected void onObject(Object o) {
                if(o instanceof GameState) turns.add((GameState)o);
            }
        };
        client.start();
        synchronized(turns) { turns.wait(); }
    }

    private static void dumpInfo() {
        System.out.println("* Board: ");
        match.getBoardY().debug();

        match.getBoardY().getEval().setupForY();
        match.getBoardY().getEval().evaluateY(Board.YPLAYER);
        match.getBoardX().getEval().setupForY();
        match.getBoardX().getEval().evaluateY(Board.YPLAYER);

        System.out.println("* Player: " + ((match.getCurrentPlayer() == Board.YPLAYER) ? "y" : "x"));
        System.out.println("* Eval: " + FindMove.getFindMove().evaluatePosition());

        System.out.println("* Value for Y [next=XPLAYER]: " + match.getBoardY().getEval().valueOfY(false, Board.XPLAYER));
        System.out.println("* Value for Y [next=YPLAYER]: " + match.getBoardY().getEval().valueOfY(true, Board.YPLAYER));
        System.out.print("* Critical positions for Y: ");
        for(Object o : match.getBoardY().getEval().getCritical()) {
            Evaluation.CritPos crit = (Evaluation.CritPos)o;
            System.out.print(crit.getY() + "|" + crit.getX() + "(" + crit.isDir() + ") ");
        }
        System.out.println("");

        System.out.println("* Value for X [next=XPLAYER]: " + match.getBoardX().getEval().valueOfY(false, Board.XPLAYER));
        System.out.println("* Value for X [next=YPLAYER]: " + match.getBoardX().getEval().valueOfY(true, Board.YPLAYER));
        System.out.print("* Critical positions for X: ");
        for(Object o : match.getBoardX().getEval().getCritical()) {
            Evaluation.CritPos crit = (Evaluation.CritPos)o;
            System.out.print(crit.getX() + "|" + crit.getY() + "(" + crit.isDir() + ") ");
        }
        System.out.println("");

        System.out.println("* Evaluation State X: ");
        match.getBoardX().getEval().debug(true);
        System.out.println("* Evaluation State Y: ");
        match.getBoardY().getEval().debug(false);
    }

    private static void initializeMatch() {
        CheckPattern.getInstance().loadPattern();
        Zobrist.getInstance().initialize();
        match = new Match();

        GameState initGameState = turns.get(0);
        MatchData md = new MatchData();
        md.mdYstarts = true;
        md.mdXhuman = true;
        md.mdYhuman = true;
        ArrayList<Move> swamps = new ArrayList<>();
        for(int y = 0; y < 24; ++y) {
            for(int x = 0; x < 24; ++x) {
                if(initGameState.getBoard().getField(x,y).getType() == FieldType.SWAMP) {
                    swamps.add(new Move(x,y));
                }
            }
        }
        match.prepareNewMatch(md, swamps);
    }

    private static void testMove(int y, int x) {
        System.out.println("*** Test move: " + y + "|" + x);
        match.setlastMove(x,y);
        examinePrincipalVariation(4);
        match.removeSingleMove();
    }

    private static void examinePrincipalVariation(int maxlength) {
        System.out.println("* Principal variation up to length " + maxlength + ":");
        for(int maxdepth = 2; maxdepth <= maxlength; ++maxdepth) {
            System.out.print("  - depth " + maxdepth + ": ");
            int curdepth = maxdepth;
            do {
                FindMove.getFindMove().prepareSearch(match.getCurrentPlayer());
                FindMove.getFindMove().setStopPly(curdepth + 1);
                withSuppressStdout(FindMove.getFindMove());

                Move best = FindMove.getFindMove().getCurrentBestMove();
                System.out.print(best.getY() + "|" + best.getX() + " ");
                match.setlastMove(best.getX(), best.getY());
            } while(--curdepth > 0);
            System.out.println("");
            if(maxdepth == maxlength) {
                System.out.println("* Expected final state:");
                withIndentStdout(Analyze::dumpInfo);
            }
            for(int i = 0; i < maxdepth; ++i) match.removeSingleMove();
        }
    }

    public static void main(String args[]) throws Exception {
        String file = "/data/code/sct1j/replays/current.xml.gz";
        final int round = 7;
        final boolean redTurn = true;

        int numberOfMovesToReplay = round * 2 - (redTurn ? 2 : 1);

        System.out.println("*** Load replay: " + file );
        loadReplay(file);
        System.out.println("*** Loaded " + turns.size() + " turns.");

        System.out.println("*** Initialize T1J match data structures");
        initializeMatch();

        System.out.print("*** Replay moves: ");
        int move = 1;
        for(; move <= numberOfMovesToReplay; ++move) {
            GameState gameState = turns.get(move);
            sc.plugin2016.Move last = gameState.getLastMove();
            match.setlastMove(last.getX(), last.getY());
            System.out.print(last.getY() + "|" + last.getX() + " ");
        }
        System.out.println("");

        System.out.println("*** General turn info " + " [Runde " + round + ", " + (redTurn ? "Rot" : "Blau") + " am Zug]:");

        System.out.println("* Board:");
        match.getBoardY().debug();

        sc.plugin2016.Move played = turns.get(move).getLastMove();
        System.out.println("* Played move: " + played.getY() + ":" + played.getX());

        dumpInfo();

        OrderedMoves moveSet = new OrderedMoves(match);
        moveSet.generateMoves(match.getCurrentPlayer(), false);
        System.out.print("* Tried moves: ");
        {
            Move m;
            while ((m = moveSet.getMove()) != null) {
                System.out.print(m.getY() + "|" + m.getX() + " ");
            }
        }
        System.out.println("");
        System.out.println("* Player: " + ((match.getCurrentPlayer() == Board.XPLAYER) ? "x" : "y"));
        examinePrincipalVariation(5);
        testMove(8,14);
        testMove(10,8);
    }
}
