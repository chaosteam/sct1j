/***************************************************************************
 * *
 * This program is free software; you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation; either version 2 of the License, or     *
 * (at your option) any later version.                                   *
 * *
 ***************************************************************************/
package net.schwagereit.t1j;

import java.util.prefs.Preferences;

/**
 * Store all the data of a match, but not the moves.
 * @author Johannes Schwagereit (mail(at)johannes-schwagereit.de)
 */
public final class MatchData implements Cloneable {
    public boolean mdYhuman = true;
    public boolean mdXhuman = true;
    public boolean mdYstarts = true;
    public boolean mdGameOver = false;


    /**
     * Cons'tor.
     */
    public MatchData() {
    }

    /** (non-Javadoc)
     * @see java.lang.Object#clone()
     */
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
