/***************************************************************************
 * *
 * This program is free software; you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation; either version 2 of the License, or     *
 * (at your option) any later version.                                   *
 * *
 ***************************************************************************/
package net.schwagereit.t1j;

import java.util.List;
import java.util.ArrayList;

/**
 * Representation of playing boards. Board is implemented as singleton
 *
 * @author Johannes Schwagereit (mail(at)johannes-schwagereit.de)
 *
 */
public final class Board {

    /** swamp */
    public static final int SWAMP = 2;
    /** playing East-West. */
    public static final int XPLAYER = 1;
    /** playing North-South. */
    public static final int YPLAYER = -1;
    /** size of the board. */
    public static final int SIZE = 24;
    private static final Board BOARD_Y = new Board(); // this is the real board
    private static final Board BOARD_X = new Board(); // this board is mirrored on x/y-axis
    /** border around field. */
    public static final int MARGIN = 3;
    /** value of bridge if bridge was layed. */
    private static final int BRIDGED = 10;
    /** Multiplier */
    private static final int MULT = 1000;
    /** the Zobristmatrix. */
    private static final Zobrist ZOBRIST = Zobrist.getInstance();
    private final Node[][] field = new Node[SIZE + MARGIN * 2][SIZE + MARGIN * 2];
    /** This board used for zobrist-hashing. */
    private boolean zobristEnabled;


    /** Value of board for zobrist-hashing. */
    private int zobristValue;

    private Evaluation eval;

    /**
     * Cons'tor - no external instance.
     */
    private Board() { //some initialization
        int i, j;
        for (i = 0; i < field.length; i++)
            for (j = 0; j < field[i].length; j++) {
                field[i][j] = new Node();
            }
        setEval(new Evaluation(this));
        zobristEnabled = false;
        zobristValue = 0;
    }

    /**
     * Get one of the two existing board.
     * (Sort of factory pattern.)
     *
     * @param player orientation of the board
     * @return the Board
     */
    public static Board getBoard(final int player) {
        if (player == XPLAYER) {
            return BOARD_X; // mirrored board
        } else {
            return BOARD_Y; // main board
        }
    }

    /**
     * calculate ending point of bridge.
     *
     * @param fromx
     *           start-x
     * @param fromy
     *           start-y
     * @param direction
     *           direction of bridge
     * @return x * 1000 + y
     */
    public static int bridgeEnd(final int fromx, final int fromy, final int direction) {
        int tox, toy;

        switch (direction) {
            case 0:
                tox = fromx - 2;
                toy = fromy - 1;
                break;
            case 1:
                tox = fromx - 1;
                toy = fromy - 2;
                break;
            case 2:
                tox = fromx + 1;
                toy = fromy - 2;
                break;
            case 3:
                tox = fromx + 2;
                toy = fromy - 1;
                break;
            default:
                throw new IllegalArgumentException("Unsupported direction");
        }
        return tox * MULT + toy;
    }

    /**
     * Clear the board between two games.
     */
    public void initializeBoard(List<Move> swamps) {
        int i, j;
        for (i = 0; i < field.length; i++)
            for (j = 0; j < field[i].length; j++) {
                field[i][j].clear();
            }
        for (Move swamp : swamps) {
            field[swamp.getX() + MARGIN][swamp.getY() + MARGIN].value = SWAMP;
        }
        zobristValue = 0;
    }

    /**
     * Add a bridge. Works with corrected data (+ MARGIN)
     *
     * @param x
     *           x
     * @param y
     *           y
     * @param direction
     *           Allowed values are 0 to 3
     */
    private void setBridge(final int x, final int y, final int direction) {
        if (direction > 3 || direction < 0) //DEBUG
        {
            throw new IllegalArgumentException("direction " + direction + " is not allowed.");
        }
        if (field[x][y].bridge[direction] > 0) {
            return; //bridge cannot be layed because something is blocking me
        }
        field[x][y].bridge[direction] = BRIDGED;
        zobristValue ^= ZOBRIST.getLinkValue(x, y, direction);

        //now mark all the 9 crossing bridges as illegal
        switch (direction) {
            case 0:
                field[x][y + 1].bridge[1]++;
                field[x - 1][y + 1].bridge[2]++;
                field[x - 2][y + 1].bridge[2]++;
                field[x - 1][y].bridge[3]++;
                field[x - 1][y].bridge[2]++;
                field[x - 1][y].bridge[1]++;
                field[x - 2][y].bridge[3]++;
                field[x - 2][y].bridge[2]++;
                field[x - 3][y].bridge[3]++;
                break;
            case 1:
                field[x - 1][y + 1].bridge[2]++;
                field[x + 1][y].bridge[0]++;
                field[x - 1][y].bridge[3]++;
                field[x - 1][y].bridge[2]++;
                field[x - 2][y].bridge[3]++;
                field[x][y - 1].bridge[0]++;
                field[x - 1][y - 1].bridge[3]++;
                field[x - 1][y - 1].bridge[2]++;
                field[x - 2][y - 1].bridge[3]++;
                break;
            case 2:
                field[x + 1][y + 1].bridge[1]++;
                field[x - 1][y].bridge[3]++;
                field[x + 1][y].bridge[0]++;
                field[x + 1][y].bridge[1]++;
                field[x + 2][y].bridge[0]++;
                field[x][y - 1].bridge[3]++;
                field[x + 1][y - 1].bridge[0]++;
                field[x + 1][y - 1].bridge[1]++;
                field[x + 2][y - 1].bridge[0]++;
                break;
            case 3:
                field[x][y + 1].bridge[2]++;
                field[x + 1][y + 1].bridge[1]++;
                field[x + 2][y + 1].bridge[1]++;
                field[x + 1][y].bridge[0]++;
                field[x + 1][y].bridge[1]++;
                field[x + 1][y].bridge[2]++;
                field[x + 2][y].bridge[0]++;
                field[x + 2][y].bridge[1]++;
                field[x + 3][y].bridge[0]++;
                break;
            default:
                throw new IllegalArgumentException("direction illegal");
        }
    }

    /**
     * Remove bridge from board.
     *
     * @param x
     *           x
     * @param y
     *           y
     * @param direction
     *           direction of bridge
     */
    private void removeBridge(final int x, final int y, final int direction) {
        if (field[x][y].bridge[direction] < BRIDGED) {
            return; //there was no bridge
        }
        field[x][y].bridge[direction] = 0;
        zobristValue ^= ZOBRIST.getLinkValue(x, y, direction);

        //now remove mark for the 9 crossing
        switch (direction) {
            case 0:
                field[x][y + 1].bridge[1]--;
                field[x - 1][y + 1].bridge[2]--;
                field[x - 2][y + 1].bridge[2]--;
                field[x - 1][y].bridge[3]--;
                field[x - 1][y].bridge[2]--;
                field[x - 1][y].bridge[1]--;
                field[x - 2][y].bridge[3]--;
                field[x - 2][y].bridge[2]--;
                field[x - 3][y].bridge[3]--;
                break;
            case 1:
                field[x - 1][y + 1].bridge[2]--;
                field[x + 1][y].bridge[0]--;
                field[x - 1][y].bridge[3]--;
                field[x - 1][y].bridge[2]--;
                field[x - 2][y].bridge[3]--;
                field[x][y - 1].bridge[0]--;
                field[x - 1][y - 1].bridge[3]--;
                field[x - 1][y - 1].bridge[2]--;
                field[x - 2][y - 1].bridge[3]--;
                break;
            case 2:
                field[x + 1][y + 1].bridge[1]--;
                field[x - 1][y].bridge[3]--;
                field[x + 1][y].bridge[0]--;
                field[x + 1][y].bridge[1]--;
                field[x + 2][y].bridge[0]--;
                field[x][y - 1].bridge[3]--;
                field[x + 1][y - 1].bridge[0]--;
                field[x + 1][y - 1].bridge[1]--;
                field[x + 2][y - 1].bridge[0]--;
                break;
            case 3:
                field[x][y + 1].bridge[2]--;
                field[x + 1][y + 1].bridge[1]--;
                field[x + 2][y + 1].bridge[1]--;
                field[x + 1][y].bridge[0]--;
                field[x + 1][y].bridge[1]--;
                field[x + 1][y].bridge[2]--;
                field[x + 2][y].bridge[0]--;
                field[x + 2][y].bridge[1]--;
                field[x + 3][y].bridge[0]--;
                break;
            default:
                throw new IllegalArgumentException("direction illegal");
        }
    }

    /**
     * set a pin with bondary-checks. Input data is corrected (+MARGIN).
     *
     * @param xin
     *           x
     * @param yin
     *           y
     * @param player
     *           XPLAYER or YPLAYER
     * @return setting pin was successful
     */
    public boolean setPin(final int xin, final int yin, final int player) {
        if (!pinAllowed(xin, yin, player)) {
            return false;
        }

        if (zobristEnabled) {
            zobristValue ^= ZOBRIST.getPinValue(xin, yin, player);
        }

        int x = xin + MARGIN; //the input data is corrected to reflect the margin
        int y = yin + MARGIN;
        //checks are done, so lets move on
        field[x][y].value = player;
        //checks for bridges if the other pin has the same color
        if (field[x - 2][y - 1].value == player) {
            setBridge(x, y, 0);
        }
        if (field[x - 1][y - 2].value == player) {
            setBridge(x, y, 1);
        }
        if (field[x + 1][y - 2].value == player) {
            setBridge(x, y, 2);
        }
        if (field[x + 2][y - 1].value == player) {
            setBridge(x, y, 3);
        }
        if (field[x + 2][y + 1].value == player) {
            setBridge(x + 2, y + 1, 0);
        }
        if (field[x + 1][y + 2].value == player) {
            setBridge(x + 1, y + 2, 1);
        }
        if (field[x - 1][y + 2].value == player) {
            setBridge(x - 1, y + 2, 2);
        }
        if (field[x - 2][y + 1].value == player) {
            setBridge(x - 2, y + 1, 3);
        }

        eval.addForY(xin, yin);
        return true; //set was okay
    }

    /**
     * remove a set pin.
     *
     * @param xin
     *           x
     * @param yin
     *           y
     * @param player player
     * @return true if okay
     */
    public boolean removePin(final int xin, final int yin, final int player) {
        int x = xin + MARGIN; //the input data is corrected to reflect the margin
        int y = yin + MARGIN;
        if (field[x][y].value == 0) //empty field?
        {
            return false;
        }

        if (zobristEnabled) {
            zobristValue ^= ZOBRIST.getPinValue(xin, yin, player);
        }

        //checks are done, so lets move on
        field[x][y].value = 0;
        //checks for bridges if the other pin has the same color
        removeBridge(x, y, 0);
        removeBridge(x, y, 1);
        removeBridge(x, y, 2);
        removeBridge(x, y, 3);
        removeBridge(x + 2, y + 1, 0);
        removeBridge(x + 1, y + 2, 1);
        removeBridge(x - 1, y + 2, 2);
        removeBridge(x - 2, y + 1, 3);

        eval.removeForY(xin, yin, player);

        return true; //remove was okay
    }

    /**
     * Check if it would be allowed to set specified pin.
     *
     * @param xin
     *           x
     * @param yin
     *           y
     * @param player
     *           player
     * @return true, if pin allowed
     */
    public boolean pinAllowed(final int x, final int y, final int player) {
        return (( player == XPLAYER &&
		  0 <= x && x < SIZE && 1 <= y && y < SIZE - 1 ) ||
                ( player == YPLAYER &&
		  1 <= x && x < SIZE - 1 && 0 <= y && y < SIZE )    ) &&
               field[x + MARGIN][y + MARGIN].value == 0;

    }

    /**
     * Check if two points are connected by bridge.
     *
     * @param xa
     *           x of first pin
     * @param ya
     *           y of first pin
     * @param xb
     *           x of second pin
     * @param yb
     *           y of second pin
     * @return true, if connected
     */
    public boolean isConnected(final int xa, final int ya, final int xb, final int yb) {
        if ( Math.abs(xa - xb) >= 3 || Math.abs(ya - yb) >= 3 ||
             Math.abs(xa - xb) + Math.abs(ya - yb) != 3          ) //DEBUG?
        {
            throw new IllegalArgumentException("Wrong distance");
        }
        //data is NOT corrected (margin!) here because isBridged corrects them
        //put lower first
        if (ya < yb) {
            return isBridged(xb, yb, (xa < xb) ? xa - xb + 2 : xa - xb + 1);
        } else {
            return isBridged(xa, ya, (xb < xa) ? xb - xa + 2 : xb - xa + 1);
        }
    }

    /**
     * Check if bridge allowed betweentwo points.
     *
     * @param xa x of first pin
     * @param ya y of first pin
     * @param xb x of second pin
     * @param yb y of second pin
     * @return true, if connected
     */
    public boolean isBridgeAllowed(final int xa, final int ya, final int xb, final int yb) {
        if (Math.abs(xa - xb) >= 3 || Math.abs(ya - yb) >= 3
                || Math.abs(xa - xb) + Math.abs(ya - yb) != 3) //DEBUG?
        {
            throw new IllegalArgumentException("Wrong distance");
        }
        //data is NOT corrected (margin!) here because isBridged corrects them
        //put lower first
        if (ya < yb) {
            return bridgeAllowed(xb, yb, (xa < xb) ? xa - xb + 2 : xa - xb + 1);
        } else {
            return bridgeAllowed(xa, ya, (xb < xa) ? xb - xa + 2 : xb - xa + 1);
        }
    }

    /**
     * get Value of pin a (x,y).
     *
     * @param x
     *           x
     * @param y
     *           y
     * @return 0 (==empty) or owner of pin
     */
    public int getPin(final int x, final int y) {
        //the input data is corrected to reflect the margin
        return field[x + MARGIN][y + MARGIN].value;
    }

    public List<Pin> getBridgedPins(Pin pin) {
	List<Pin> result = new ArrayList<>(4); //more than four briges at one pin are very uncommon
	int x = pin.x;
        int y = pin.y;

        if (isBridged(x, y, 0))
            result.add(new Pin(x - 2, y - 1));
        if (isBridged(x, y, 1))
            result.add(new Pin(x - 1, y - 2));
        if (isBridged(x, y, 2))
            result.add(new Pin(x + 1, y - 2));
        if (isBridged(x, y, 3))
            result.add(new Pin(x + 2, y - 1));
        if (isBridged(x + 2, y + 1, 0))
            result.add(new Pin(x + 2, y + 1));
        if (isBridged(x + 1, y + 2, 1))
            result.add(new Pin(x + 1, y + 2));
        if (isBridged(x - 1, y + 2, 2))
            result.add(new Pin(x - 1, y + 2));
        if (isBridged(x - 2, y + 1, 3))
            result.add(new Pin(x - 2, y + 1));

        return result;
    }

    /**
     * Check if specified bridge exists.
     *
     * @param x
     *           x
     * @param y
     *           y
     * @param direction
     *           direction
     * @return true, if bridge exists
     */
    public boolean isBridged(final int x, final int y, final int direction) {
        //the input data is corrected to reflect the margin
        return field[x + MARGIN][y + MARGIN].bridge[direction] >= Board.BRIDGED;
    }

    /**
     * Check if specified bridge is allowed, but does not exists.
     *
     * @param x
     *           x
     * @param y
     *           y
     * @param direction
     *           direction
     * @return true, if bridge allowed
     */
    public boolean bridgeAllowed(final int x, final int y, final int direction) {
        //the input data is corrected to reflect the margin
        return field[x + MARGIN][y + MARGIN].bridge[direction] == 0;
    }

    /**
     * A Pin is strong if it is connected to any other pin.
     * @param hx x
     * @param hy y
     * @return true if pin is strong
     */
    public boolean isStrong(int hx, int hy) {
        return isBridged(hx, hy, 0) || isBridged(hx, hy, 1) || isBridged(hx, hy, 2)
                || isBridged(hx, hy, 3) || isBridged(hx + 2, hy + 1, 0) ||
                isBridged(hx + 1, hy + 2, 1) || isBridged(hx - 1, hy + 2, 2) ||
                isBridged(hx - 2, hy + 1, 3);
    }

    /**
     * Get the Evaluation-object for this board.
     * @return Returns the eval.
     */
    public Evaluation getEval() {
        return eval;
    }

    /**
     * Set the Evaluation-object for this board.
     * @param evalIn The eval to set.
     */
    private void setEval(final Evaluation evalIn) {
        this.eval = evalIn;
    }

    /**
     * Check if game is over for the evaluation connected with this board.
     * @return true, if game is over
     */
    public boolean checkGameOver() {
        eval.evaluateY(Board.XPLAYER);
        return (eval.valueOfY(false, Board.XPLAYER) == 0);
    }

    /**
     * En/disable zobrist hashing.
     * @param zobristEnabledIn Value
     */
    public void setZobristEnabled(boolean zobristEnabledIn) {
        this.zobristEnabled = zobristEnabledIn;
    }

    /**
     * get current zobrist value.
     * @return int
     */
    public int getZobristValue() {
        return zobristValue;
    }

    public void debug() {
        System.out.print("   ");
        for(int i = 0; i < 24; ++i) {
            System.out.format("%02d ", i);
        }
        System.out.print("\n");
        for(int i = 0; i < 24; ++i) {
            System.out.format("%02d ", i);
            for(int j = 0; j < 24; ++j) {
                switch(this.getPin(j,i)) {
                    case 0: System.out.print("."); break;
                    case XPLAYER: System.out.print("x"); break;
                    case YPLAYER: System.out.print("y"); break;
                    case SWAMP: System.out.print("s"); break;
                }
                System.out.print("  ");
            }
            System.out.print("\n");
        }
    }

    final private class Node {
        private final int[] bridge = new int[4];
        private int value; //0 or XPLAYER or YPLAYER or SWAMP

        /**
         * Initialize a node.
         */
        void clear() {
            value = 0;
            bridge[0] = 0;
            bridge[1] = 0;
            bridge[2] = 0;
            bridge[3] = 0;
        }

        /**
         * Copy all values from a source-node to another node.
         * @param source the source
         */
        public void getValues(Node source) {
            value = source.value;
            bridge[0] = source.bridge[0];
            bridge[1] = source.bridge[1];
            bridge[2] = source.bridge[2];
            bridge[3] = source.bridge[3];
        }
    }
}
