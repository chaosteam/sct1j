/***************************************************************************
 * *
 * This program is free software; you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation; either version 2 of the License, or     *
 * (at your option) any later version.                                   *
 * *
 ***************************************************************************/
package net.schwagereit.t1j;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Store all the data of a match.
 * @author Johannes Schwagereit (mail(at)johannes-schwagereit.de)
 *
 */
public final class Match {
    private static final int INIT_ARRAY_SZ = 20;
    /** the real board. */
    private final Board boardY;
    /** the mirrored board. */
    private final Board boardX;
    /** all the moves played yet. */
    private final List<Move> moves = new ArrayList(INIT_ARRAY_SZ);
    private MatchData matchData;
    /** next player to place a pin. */
    private int nextPlayer;
    /** highest move number already used in this game. */
    private int highestMoveNr;

    /** current move number. */
    private int moveNr;

    /**
     * Cons'tor for new match.
     */
    public Match() {
        boardY = Board.getBoard(Board.YPLAYER);
        boardY.setZobristEnabled(true);
        boardX = Board.getBoard(Board.XPLAYER);
        matchData = null;
        //evaluationY = new Evaluation(Board.YPLAYER);
        //evaluationX = new Evaluation(Board.XPLAYER);
        FindMove.getFindMove().setMatch(this);
    }

    /**
     * prepare a new match.
     *  @param md Data of new game
     * @param swamps list of swamps on the board
     */
    public void prepareNewMatch(final MatchData md, List<Move> swamps) {
        this.matchData = md;
        nextPlayer = matchData.mdYstarts ? Board.YPLAYER : Board.XPLAYER;
        boardY.initializeBoard(swamps);
        boardX.initializeBoard(swamps.stream().map(Move::getMirror).collect(Collectors.toList()));
        moves.clear();
        moveNr = 0;
        highestMoveNr = 0;

        boardY.getEval().setupForY();
        boardX.getEval().setupForY();
    }

    /**
     * Get current move-nr.
     * @return current move-nr
     */
    int getMoveNr() {
        return moveNr;
    }

    /**
     * Get highest move-nr used in this match.
     * @return highest move-nr used in this match
     */
    int getHighestMoveNr() {
        return highestMoveNr;
    }

    /**
     * Get x-value of specified move.
     * @param nr move-nr
     * @return x of move nr
     */
    int getMoveX(final int nr) {
        return ((Move) moves.get(nr - 1)).getX();
    }

    /**
     * Get y-value of specified move.
     * @param nr move-nr
     * @return y of move nr
     */
    int getMoveY(final int nr) {
        return ((Move) moves.get(nr - 1)).getY();
    }

    /**
     * A move was done, now some data has to be updated.
     *
     * @param xin x
     * @param yin y
     * @return true, if move okay
     */
    public boolean setlastMove(final int xin, final int yin) {
        if (boardY.setPin(xin, yin, nextPlayer)) // a legal move?
        {
            // set same pin on mirrored board
            if (!boardX.setPin(yin, xin, -nextPlayer)) {  // should never happen
                throw new IllegalStateException("Error when setting pin " + yin
                        + ":" + xin + " on  mirrorboard.");
            }

            //switch players
            nextPlayer = -nextPlayer;

            //addElement to list of moves
            if (moves.size() > moveNr) {
                moves.get(moveNr).setX(xin);
                moves.get(moveNr).setY(yin);
            } else {
                moves.add(moveNr, new Move(xin, yin));
            }
            moveNr++;
            highestMoveNr = moveNr;

            return true;
        } else {  // move was not legal
            return false;
        }

    }

    /**
     * Undo a single move.
     */
    public void removeSingleMove()
    {
        moveNr--;
        Move thisMove = (Move) moves.get(moveNr);
        if (boardY.removePin(thisMove.getX(), thisMove.getY(), -nextPlayer)) // a legal remove?
        {
            boardX.removePin(thisMove.getY(), thisMove.getX(), nextPlayer);

            nextPlayer = -nextPlayer;
        }
    }

    /**
     * Get Board.
     * @return Returns the board.
     */
    public Board getBoardY() {
        return boardY;
    }

    /**
     * Get mirrored Board.
     * @return Returns the board.
     */
    public Board getBoardX() {
        return boardX;
    }

    /**
     * Get Matchdata.
     * @return Returns the matchData.
     */
    public MatchData getMatchData() {
        return matchData;
    }

    /**
     * Get Board for specified player.
     * @param player x- or y-player
     * @return Returns the board.
     */
    public Board getBoard(final int player) {
        return (player == Board.XPLAYER) ? boardX : boardY;
    }

    public int getCurrentPlayer() {
        return this.nextPlayer;
    }
}
