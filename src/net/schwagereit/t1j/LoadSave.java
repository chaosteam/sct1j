/***************************************************************************
 * *
 * This program is free software; you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation; either version 2 of the License, or     *
 * (at your option) any later version.                                   *
 * *
 ***************************************************************************/
package net.schwagereit.t1j;

import java.io.*;
import java.util.ArrayList;

/**
 * Load and Save files of T1-format.
 * This is a 'static' class
 *
 * Created by IntelliJ IDEA.
 * @author Johannes Schwagereit (mail(at)johannes-schwagereit.de)
 */
public final class LoadSave {
    /** constant for newline. */
    private static final String LINESEP = System.getProperty("line.separator");

    private static BufferedReader reader;
    private static BufferedWriter writer;

    /**
     * private constructor.
     */
    private LoadSave() {
    }

    /**
     * Read a line from the file if possible.
     * @return String or null, if nothing found
     */
    private static String getLine() {
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                int pos = line.indexOf("#");
                if (pos >= 0) {
                    line = line.substring(0, pos);
                }
                line = line.trim();
                if (line.length() > 0) {
                    return line;
                }
            }
        } catch (IOException e) {
            System.out.println("IO-Error reading file.");
        }
        return null;
    }

    /**
     * Read Data of saved game.
     * @param match The match to fill
     */
    private static void getGameData(final Match match) throws IOException {
        MatchData loadData = new MatchData();

        getLine(); // version of file-format (ignored)

        loadData.mdYhuman = (getLine().charAt(0) == 'H');
        loadData.mdXhuman = (getLine().charAt(0) == 'H');

        loadData.mdYstarts = (getLine().charAt(0) == '1');

        getLine(); // mdLetterDir (ignored)
        loadData.mdGameOver = (getLine().charAt(0) == 'Y');

        match.prepareNewMatch(loadData, new ArrayList<>());
        // the meta-data was loaded, now lets start with the moves
        String line;
        while ((line = getLine()) != null) {
            String[] parts = line.split(":");
            match.setlastMove(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
        }
    }

    /**
     * Write Data to file.
     * @param match The match to write
     */
    private static void writeGameData(final Match match) throws IOException {
        MatchData saveData = match.getMatchData();

        writer.write("# File created by T1j" + LINESEP);
        writer.write(
                "# T1j is a program to play TwixT (mail@johannes-schwagereit.de)" + LINESEP);
        writer.write("1 # version of file-format" + LINESEP);

        writer.write(
                (saveData.mdYhuman ? "H" : "C") + "# player 1 human or computer" + LINESEP);
        writer.write(
                (saveData.mdXhuman ? "H" : "C") + "# player 2 human or computer" + LINESEP);
        writer.write((saveData.mdYstarts ? "1" : "2")
                + "# starting player (1 plays top-down)" + LINESEP);
        writer.write("V# Direction of letters" + LINESEP);
        writer.write((saveData.mdGameOver ? "Y" : "N") + "# game already over?" + LINESEP);

        //write moves
        int x, y;
        for (int i = 1; i <= match.getMoveNr(); i++) {
            x = match.getMoveX(i);
            y = match.getMoveY(i);
            writer.write("" + x + ":" + y + LINESEP);
        }
    }


    /**
     * Load a saved game.
     *
     * @param match The match to fill
     */
    static void loadGame(final Match match, File file) {
        boolean status = true;
        String message = "";
        try {

            reader = new BufferedReader(new FileReader(file));

            getGameData(match);

            reader.close();
        } catch (FileNotFoundException e) {
            message = "File '" + file.getAbsolutePath() + "' not found.";
            status = false;
        } catch (IOException e) {
            message = "IO-Error reading file '" + file.getAbsolutePath() + "'.";
            status = false;
        } finally {
            reader = null;
        }
    }

    /**
     * Save a game.
     *
     * @param match The match to save
     */
    static void saveGame(final Match match, File file) {
        boolean status = true;
        String message = "";
        try {
            writer = new BufferedWriter(new FileWriter(file));

            writeGameData(match);

            writer.close();
        } catch (IOException e) {
            message = "IO-Error writing file '" + file.getAbsolutePath() + "'.";
            status = false;
        } finally {
            writer = null;
        }
    }

}
