/***************************************************************************
 * *
 * This program is free software; you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation; either version 2 of the License, or     *
 * (at your option) any later version.                                   *
 * *
 ***************************************************************************/
package net.schwagereit.t1j;

import java.util.HashMap;
import java.util.Map;


/**
 * Generate all moves and try to find best one. This class is a singleton.
 *
 * @author Johannes Schwagereit (mail(at)johannes-schwagereit.de)
 */
public final class FindMove implements Runnable {
    private static final int MILLI_PER_SEC = 1000;
    private static final int GAMEOVER = MILLI_PER_SEC;
    private static final FindMove FINDMOVES = new FindMove();
    private static final int INITIAL_CAPACITY = 10000;
    private static Match match = null;
    /** The searchddepth currently used */
    private static int currentMaxPly;
    private final Map<Integer, Float> zobristMap = new HashMap<>(INITIAL_CAPACITY);
    /** the bestMove found yet */
    private Move bestMove;
    private int startPlayer;
    private int leavesSearched;
    private int nodesSearched;
    private int stopPly = Integer.MAX_VALUE;


    /**
     * Cons'tor - no external instance.
     */
    private FindMove() {
    }

    /**
     * Return the FindMove-Object. (Singleton)
     *
     * @return Findmove-Object
     */
    public static FindMove getFindMove() {
        return FINDMOVES;
    }

    /** Prepare the data structures for a new search run.
     *
     * @param player The player whose turn it is
     */
    public void prepareSearch(final int player) {
        this.startPlayer = player;
        this.bestMove = null;
    }

    public Move getCurrentBestMove() {
        return bestMove;
    }


    /**
     * Find best move for computerplayer by using alpha-beta.
     */
    public void run() {
        // the first pins are set by simple rules
        Move initMove = InitialMoves.getInstance().initialMove(match, startPlayer);
        if (initMove != null) {
            bestMove = initMove;
            return;
        } else {
            bestMove = OrderedMoves.randomMove(startPlayer, match);
        }

        // use alpha-beta
        OrderedMoves.initOrderedMoves();
        zobristMap.clear();
        for (currentMaxPly = 1; !Thread.currentThread().isInterrupted() && currentMaxPly < stopPly; currentMaxPly++) {
            System.out.println("- Current ply: " + currentMaxPly);
            leavesSearched = 0;
            nodesSearched = 0;
            // if (currentMaxPly != 4) {
            alphaBeta(startPlayer, currentMaxPly, -Integer.MAX_VALUE, Integer.MAX_VALUE);
            // }
            System.out.println("    Leaves searched: " + leavesSearched);
            System.out.println("    Nodes searched: " + nodesSearched);
            System.out.println("    Best move: " + bestMove.getY() + "|" + bestMove.getX());
        }
    }


    /**
     * Set the match.
     *
     * @param matchIn The match to set.
     */
    public void setMatch(final Match matchIn) {
        FindMove.match = matchIn;
    }


    /**
     * Evaluate situation.
     * @return value of current situation on board - the higher the better for start player
     */
    public float evaluatePosition() {
        float ratingX, ratingY;

        // evaluate
        ratingX = match.getBoardX().getEval().evaluate();
        ratingY = match.getBoardY().getEval().evaluate();

        if (match.getMoveNr() < 8) { // at the first move only defensive moves are good
	    if ( startPlayer == Board.XPLAYER )
		return ratingY;
	    else
		return - ratingX;
	}
        return ratingY - ratingX;
    }

    /**
     * Make a move on both boards.
     * @param move the move
     * @param player the player
     */
    private void makeMove(Move move, int player) {
        match.getBoardY().setPin(move.getX(), move.getY(), player);
        match.getBoardX().setPin(move.getY(), move.getX(), -player);
    }


    /**
     * Unmake a move on both boards.
     * @param move the move
     * @param player the player
     */
    private void unmakeMove(Move move, int player) {
        match.getBoardY().removePin(move.getX(), move.getY(), player);
        match.getBoardX().removePin(move.getY(), move.getX(), -player);
    }

    /**
     * Recursive minimax with alpha-beta pruning to find best move.
     * @param player player who has next turn
     * @param ply current depth, decreasing
     * @param alpha alpha-value
     * @param beta beta-value
     * @return value computed
     */
    private float alphaBeta(int player, int ply, float alpha, float beta) {
        float rating;
        Move move;

        nodesSearched += 1;

        if ( ply == 0 ) {
            Float zobristRating = zobristMap.get(new Integer(match.getBoardY().getZobristValue()));
            if (zobristRating != null)
                return zobristRating.floatValue();
            rating = evaluatePosition();
            leavesSearched++;
            zobristMap.put(new Integer(match.getBoardY().getZobristValue()), new Float(rating));
            return rating;
        }

        OrderedMoves moveSet = new OrderedMoves(match);
        moveSet.generateMoves(player, ply == currentMaxPly);

        // a check for game over
        if (moveSet.isGameover()) {
            // the earlier the better
            return (GAMEOVER + ply) * player;
        }

        // minimizing node
        if (player == Board.XPLAYER) {
            while ((move = moveSet.getMove()) != null && !Thread.currentThread().isInterrupted() ) {
                makeMove(move, player);
                rating = alphaBeta(-player, ply - 1, alpha, beta);

                if (ply == currentMaxPly) {
                    OrderedMoves.addValuedMove(move, rating);
                }

                if (rating < beta) {
                    if (ply == currentMaxPly && !Thread.currentThread().isInterrupted()) {
                        bestMove = move;
                        beta = rating;
                    } else {
                        beta = rating;
                        OrderedMoves.addKiller(move, player);
                    }
                }
                unmakeMove(move, player);

                if (beta <= alpha) {
                    return alpha;
                }
            }
            return beta;
        } else
        //maximizing node
        {
            while ((move = moveSet.getMove()) != null && !Thread.currentThread().isInterrupted() ) {
                makeMove(move, player);
                rating = alphaBeta(-player, ply - 1, alpha, beta);

                if (ply == currentMaxPly) {
                    OrderedMoves.addValuedMove(move, rating);
                }
                if (rating > alpha) {
                    if (ply == currentMaxPly && !Thread.currentThread().isInterrupted()) {
                        bestMove = move;
                        alpha = rating;
                    } else {
                        alpha = rating;
                        OrderedMoves.addKiller(move, player);
                    }
                }
                unmakeMove(move, player);

                if (beta <= alpha) {
                    return beta;
                }
            }
            return alpha;
        }
    }

    public int getStopPly() {
        return stopPly;
    }

    public void setStopPly(int stopPly) {
        this.stopPly = stopPly;
    }

}
