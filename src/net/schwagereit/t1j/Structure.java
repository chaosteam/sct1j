/***************************************************************************
 * *
 * This program is free software; you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation; either version 2 of the License, or     *
 * (at your option) any later version.                                   *
 * *
 ***************************************************************************/

package net.schwagereit.t1j;

import java.util.List;
import java.util.ArrayList;

public class Structure {
    public float[][] pinValues = new float[Board.SIZE][Board.SIZE];

    public Structure(Board board) {
        for (int row = 0; row < Board.SIZE; row += 1)
            for (int column = 0; column < Board.SIZE; column += 1)
                pinValues[row][column] = Float.NaN;

        List<Pin> todo, done;
        Pin pin;
        int pinStatus;
        int maximum, minimum;

        for ( int row = 0; row < Board.SIZE; row += 1 ) {
            for ( int column = 0; column < Board.SIZE; column += 1 ) {
                if ( !Float.isNaN(pinValues[row][column]) ) continue;
                pinStatus = board.getPin(column, row);
                if ( pinStatus == Board.SWAMP ) {
                    pinValues[row][column] = 0;
                } else if ( pinStatus == Board.YPLAYER ) {
                    todo = new ArrayList<>(8);
                    done = new ArrayList<>(16);
                    maximum = row;

                    todo.add(new Pin(column, row));
                    while ( !todo.isEmpty() ) {
                        pin = todo.remove(todo.size() - 1);
                        if ( pinValues[pin.y][pin.x] == 0 ) continue;

                        maximum = Math.max(maximum, pin.y);
                        pinValues[pin.y][pin.x] = 0;
                        done.add(pin);
                        todo.addAll(board.getBridgedPins(pin));
                    }
                    for ( Pin structurePin : done )
                        pinValues[structurePin.y][structurePin.x] = maximum - row + 1;
                } else if ( pinStatus == Board.XPLAYER ) {
                    todo = new ArrayList<>(8);
                    done = new ArrayList<>(16);
                    maximum = column;
                    minimum = column;

                    todo.add(new Pin(column, row));
                    while ( !todo.isEmpty() ) {
                        pin = todo.remove(todo.size() - 1);
                        if ( pinValues[pin.y][pin.x] == 0 ) continue;

                        minimum = Math.min(minimum, pin.x);
                        maximum = Math.max(maximum, pin.x);
                        pinValues[pin.y][pin.x] = 0;
                        done.add(pin);
                        todo.addAll(board.getBridgedPins(pin));
                    }
                    for ( Pin structurePin : done )
                        pinValues[structurePin.y][structurePin.x] = minimum - maximum - 1;
                }
            }
        }
    }
}
